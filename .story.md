---
focus: garage_service.py:13:old
---
### Do we really park car?
It all looks much better now, but there's still one thing bothers me.
Name of the method [park_in_free_garage](garage_service.py:13:old) doesn't seem named well.

As we know from the implementation logic, service doesn't actually park car, but more register car in a free garage depends on a car type. There's no parking actions, garage is just chosen and cleaned. What about to rename the method to `register_in_garage`?

If we do it, it's also good to rename method [register_car](garage_service.py:32) we worked on just a minute ago.

As soon as code is clean and readable now, I still could guess that
register method doesn't process all possible options. What is car was already registered, there's no such check and it could be a reason for serious defect. But it's already another story.

The end.
